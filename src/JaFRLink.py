#-------------------------------------------------------------------------------
# JaFRLink.py
#
# Date: 11/05/2017
# Author: Mike Schoonover (derived from web examples)
#
# This program accepts an ethernet connection and passes data back and forth
# between the ethernet and the serial port. The serial port can be connected
# to a hardware device such as an Arduino controlling a robot.
#
# This script also handles some commands itself from the ethernet stream to
# control and send data regarding the Pi itself.
#
# For use on Raspberry Pi.
#
# To use the serial port:
#
#   On the Desktop, open Preferences/Raspberry Pi Configuration
#   On Interfaces tab, disable Serial
#
#   In boot/config.txt, set enable_uart=1
#
# NOTE: Extra setup required for Pi models with Bluetooth!
#

#!/usr/bin/env python3

import select
import socket
import sys
import serial
import time

count = 0

#-------------------------------------------------------------------------------
# handleControllerIncoming
#
# This function reads all available bytes from the Ethernet port (controller)
# and transmits them on the serial out line (robot).
#
# Strangely (and stupidly) enough, Python does not have a simple method to
# check if bytes are available in the socket (such as Java's bytesAvailable).
# Thus, attempting to read an empty blocking socket will hang until data is
# ready or until a timeout value is reached.
#
# This issue is handled here by calling select.select which will return a list
# containing socket(s) with data ready. For this program, the socket is left
# as blocking and the normally blocking call to select is made non-blocking by
# specifying a timeout of 1. This will allow for a quick check to see if at
# least one byte is ready.
#

def handleControllerIncoming():

    global count

    inputs = [ controller ]
    outputs = [ controller ]
    errors = [ controller ]

    readReady, writeReady, in_error = select.select( inputs, outputs, errors, 1)

    while (readReady):  #{

        data = controller.recv(1)

        #print >>sys.stderr, "received from controller: %i %s" % (count, data)
        print('received from controller: %i %s' % (count, data), file=sys.stderr)

        count = count + 1;

        if data:
            robot.write(data)       # send data to robot
            time.sleep(0.005)

        readReady, writeReady, in_error = select.select( inputs, outputs, errors, 1)
        
    #}

# handleControllerIncoming
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# handleRobotIncoming
#
# This function reads all available bytes from the Serial input (robot) and
# transmits them via the Ethernet port (controller).
#

def handleRobotIncoming():

    inCount = robot.in_waiting

    while robot.in_waiting > 0:  #{

        #print >>sys.stderr, 'bytes ready from Robot: %d' % inCount
        print('bytes ready from Robot: %d' % inCount, file=sys.stderr)

        rcv = robot.read()  # read from robot port

        if len(rcv) > 0:
            v = ord(rcv)
            #print >>sys.stderr, 'received from Arduino: %d' % v
            print('received from Arduino: %d' % v, file=sys.stderr)
            controller.send(rcv)

        inCount = robot.in_waiting
        
    #}

# handleRobotIncoming
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Main Code Section
#

# create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# bind() is used to associate the socket with the server address.
# In this case, the address is '', referring to the current server,
# and the port number is 5000. 'localhost' may work in lieu of ''.

# bind the socket to the port

server_address = ('', 5000)
#print >>sys.stderr, 'starting up on %s port %s' % server_address

print('starting up on %s port %s' %server_address, file=sys.stderr)

sock.bind(server_address)

# calling listen() puts the socket into server mode, and accept() waits for an
# incoming connection

sock.listen(1)

# open serial port connection to robot

robot = serial.Serial("/dev/serial0", baudrate=57600, timeout=1.0)
print('Arduino connected...', file=sys.stderr)


while True:

    
    # wait for a connection
    #print >>sys.stderr, 'waiting for a connection'
    print('waiting for a connection...', file=sys.stderr)
    controller, client_address = sock.accept()

# accept() returns an open connection between the server and client, along with
# the address of the # client. The connection is actually a different socket on
# another port (assigned by the kernel). Data # is read from the connection
# with recv() and transmitted with sendall()

    try:

 
        #print >>sys.stderr, 'connection from', client_address
        print('connection from %s' %str(client_address), file=sys.stderr)

        message = 'Hello from Jafr!\n'
        controller.sendall(message.encode())

        # watch for data from controller and robot and forward when received

        while True:

            handleControllerIncoming()

            handleRobotIncoming()

    except socket.error:
        socketError = True

    finally:
        # clean up the connections
        controller.close()
        #print >>sys.stderr, 'TCP/IP connection closed.'
        print('TCP/IP connection closed.', file=sys.stderr)
        robot.close()
        #print >>sys.stderr, 'Serial connection closed.'
        print('Serial connection closed.', file=sys.stderr)

# Main Code Section
#-------------------------------------------------------------------------------
