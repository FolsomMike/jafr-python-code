

-- Basic Startup Sequence --

This assumes everything is properly addressed and connected. See later sections if not.

(If jafr has not been added as a Raspbian user, see section "Adding jafr as a User")

Power up the WiFi switch.

Connect laptop WiFi to the switch or use a cable...will be named: "skoonie-JaFR-1" with password "peanutter"

Turn on the robot's Arduino and Raspberry Pi. Allow them to boot. The Pi should automatically log into the router.

PuTTy into the Pi at 192.168.10.110, username "jafr" password "peanutter". 

(Before doing the above, the Pi should be configured to have a static IP address - see section "Setting the Pi's Static IP Address")

Execute:

		click the Desktop shortcut:	Start JaFR Engine & Streaming

	OR

		cd scripts

		bash jafr.sh

Open a new browser window, go to http://192.168.10.110:8000/    You should see the Pi camera's video feed.

Resize the browser window to just big enough to fit the video without scroll bars.

Run the JaFR Java host program.

Place the video browser window where it can be seen without covering the host program.

====================================================================================================================================

-- Adding JaFR as a user --

This must be done once before the programs can be executed by user "jafr".


1) SSH into Linux as a Super User (on a new Pi, the user is "pi")

2) Create the jafr user:

	sudo useradd jafr<enter>

3) Set jafr's password:

 	sudo passwd jafr<enter>
	Enter new UNIX password: peanutter<enter>
	Retype new UNIX password: peanutter<enter>

4) Create a home directory for jafr:

	sudo mkhomedir_helper jafr

5) Allow jafr to use the camera:

	sudo usermod -a -G video jafr

6) Allow jafr to use the serial port:

	sudo usermod -a -G dialout jafr

7) Set jafr up to use the Bash shell so everything works the same:

	sudo chsh -s /bin/bash jafr<enter>

	(note: if you don't do this, the Up Arrow to re-use previous commands and similar things won't work)

8) Optionally make jafr a Super User to help with debugging:

	sudo usermod -aG sudo jafr<enter>

9) Log out and log back in as jafr.

====================================================================================================================================

-- Setting the Pi's Static IP Address --


The instructions assume that the Pi has IP address 192.168.10.110

If not, then use the proper address or set the Pi up to have a static address at 192.168.10.110

You do NOT have to use that exact address...use whatever you wish but use your custom address for all operations.

NOTE: you will have to modify the Java code to match whatever IP address you choose. In the future, a configuration file will be implemented.

If your WiFi switch/router is capable, assign the address 192.168.10.110 to the Pi's mac address. The Pi can be left with dynamic IP Address assignment and the router will take care of assigning the static address.

	OR

Most modern WiFi switch/routers will honor a device's demand for a particular static IP address. Thus you can configure the Pi to use a specific static address and when it logs onto the router, the router will generally honor the demand. NOTE: make sure no other device uses the same IP address!

To configure the Pi for a static IP address:

		sudo nano /etc/dhcpcd.conf<enter>	
	
	add the following or modify the existing "interface wlan0":

		interface wlan0
		static ip_address=192.168.10.110/24
		static routers=192.168.10.1
		static domain_name_servers=192.168.10.1
	
   	The address 192.168.10.1 is the base address of the WiFi switch/router. Modify to match your switch/router
	
====================================================================================================================================

-- Installing and Setting Up the Raspberry Pi Camera for Streaming --

- Using the Python picamera Library -

This method uses the Python library picamera and the MJpeg streaming format.

Install the Python streaming script on the Pi:

	start_picamera_streaming.py

Execute it by: python3 start_picamera_streaming.py<enter>

View the stream in a web browser: http://192.168.10.102:8000/

To run the streamer in the background:

	nohup python3 start_picamera_streaming.py &<enter>


To view info about the script running in the background:

	ps ax | grep start_picamera_streaming.py<enter>

To kill the script after using the above command:

	sudo kill PID<enter>			(where PID was obtained by the prior ps command)

Quicker if you are sure no other script has the same name:

	sudo pkill -f start_picamera_streaming.py<enter>


Further resources:

 Using Python library picamera on Pi and Web browser on client:

	https://picamera.readthedocs.io/en/latest/recipes2.html#web-streaming

	https://randomnerdtutorials.com/video-streaming-with-raspberry-pi-camera/

------------------

- Using V4L2 -  <todo mks -- this section not complete! jafr does NOT currently use this method!!!>


Install the VLC Media Player on the host computer:

	https://www.videolan.org/vlc/index.html

Install the official V4L2 Raspberry Pi Cam driver bcm2835-v4l2:

	sudo modprobe -v bcm2835-v4l2

Install v4l2rtspserver:

	<todo mks -- add instructions here.

Further resources:

 Using v4l2rtspserver on Pi and VLC viewer on client:

	https://www.sylvaindurand.org/surveillance-camera-with-raspberry-pi/


====================================================================================================================================
