#!/bin/bash

echo "starting JaFR..."

nohup python3 ../python/jafr_picamera_streaming.py &

python3 ../python/JaFRLink.py

# read a dummy input to keep the command window from closing
echo "Complete. Press return to exit"
read dummy
